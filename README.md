<div align="center">
<img src="assets/logo.png" height="100" />

[![Latest Release](https://gitlab.com/ASTRELION/launchme/-/badges/release.svg)](https://gitlab.com/ASTRELION/launchme/-/releases)
[![pipeline status](https://gitlab.com/ASTRELION/launchme/badges/main/pipeline.svg)](https://gitlab.com/ASTRELION/launchme/-/commits/main)

[![bStats Servers](https://img.shields.io/bstats/servers/9918?color=00695C)](https://bstats.org/plugin/bukkit/LaunchMe/9918)
[![bStats Servers](https://img.shields.io/bstats/players/9918?color=00695C)](https://bstats.org/plugin/bukkit/LaunchMe/9918)

[![Spigot Downloads](https://img.shields.io/spiget/downloads/87512?color=%23F7C539&label=Spigot%20Downloads)](https://spigotmc.org/resources/launchme.87512)
[![Spigot Rating](https://img.shields.io/spiget/stars/87512?color=F7C539&label=Spigot%20Rating)](https://spigotmc.org/resources/launchme.87512/reviews)
[![Modrinth Downloads](https://img.shields.io/modrinth/dt/LpJBcYZv?color=1bd96a&label=Modrinth%20Downloads&logoColor=1bd96a)](https://modrinth.com/plugin/launchme)
</div>

# LaunchMe

LaunchMe is a Minecraft plugin that allows players to ride on top of any projectile they shoot or throw.

Compatible with virtually any Spigot/Paper server!

<img src="assets/launchme-usage.gif" height="200" />

**[Latest Release](https://gitlab.com/ASTRELION/launchme/-/releases/permalink/latest)**

**Official Plugin Pages**  
- [CurseForge](https://curseforge.com/minecraft/bukkit-plugins/launchmeplugin)
- [Modrinth](https://modrinth.com/plugin/launchme)
- [Paper](https://hangar.papermc.io/ASTRELION/LaunchMe)
- [Spigot](https://spigotmc.org/resources/launchme.87512/)
- [Bukkit](https://dev.bukkit.org/projects/launchmeplugin)

## Server Admins

### Installation

1. [Download the latest version of the plugin](https://gitlab.com/ASTRELION/launchme/-/releases/permalink/latest) (`launchme-x.x.x.jar`)
2. Locate your server's `plugins` folder
3. Place `launchme-x.x.x.jar` into your plugins folder
4. Restart or reload your server
- Optionally, edit the configuration files located in `plugins/LaunchMe/`

### Usage

1. Give yourself op or the `launchme.toggle` permission
2. Use `/launchme toggle` in-game to toggle projectile launching for yourself
3. By default, you need fireworks in your offhand for launching to work
4. Shoot an arrow, throw a snowball, etc. to launch!

### Configuration

Edit your configuration in the `config.yml` that was generated in the `LaunchMe` folder.

The default `config.yml` can be found [here](src/main/resources/config.yml). You may also delete your configuration and a new one will be generated on the next server restart or reload.

Descriptions of each configuration value is included in the file.

### Permissions

| Permission              | Description                                                                           |
|-------------------------|---------------------------------------------------------------------------------------|
| `launchme.*`            | grants all permissions below                                                          |
| `launchme.help`         | allows the user to use `/launchme` and `/launchme help`                               |
| `launchme.reload`       | allows the user to use `/launchme reload`                                             |
| `launchme.toggle.*`     | allows the user to toggle launching for themselves and others with `/launchme toggle` |
| `launchme.toggle`       | allows the user to toggle launching for themselves                                    |
| `launchme.toggle.other` | allows the user to toggle launching for others                                        |
| `launchme.nocooldown`   | allows the user to bypass the cooldown between launches                               |
| `launchme.nocost`       | allows the user to bypass all launching costs                                         |
| `launchme.update`       | any user with this permission will get update notifications when available            |

### Commands

| Command                     | Description                                      |
|-----------------------------|--------------------------------------------------|
| `/launchme` or `/lm`        | Base command, shows help dialog                  |
| `/launchme help [command]`  | Shows help dialog                                |
| `/launchme reload`          | Reloads `config.yml` and `lang.yml` from file    |
| `/launchme toggle [player]` | Toggles launching for yourself or another player |

*Arguments surrounded in `[]` are optional, arguments surrounded in `<>` are required.*

### Privacy

This plugin uses [bStats](https://bstats.org) to gather anonymous usage [statistics](#statistics). You can see all gathered data [here](https://bstats.org/plugin/bukkit/LaunchMe/9918) (all are standard fields for most plugins, with the exceptions of "Projectiles Launched"). You may opt out of all data collection for all plugins by editing the `/plugins/bStats/config.yml` file.

## Developers

**Please disable bStats on test servers!**

### Building

*LaunchMe requires Gradle and uses JDK 17 at the time of writing*

1. Download or clone the project.
2. Run `gradle build`
3. A new `launchme-x.x.x.jar` will be built under `build/libs/`

### Contributing

1. [Fork the repository](https://gitlab.com/ASTRELION/launchme/-/forks/new)
2. Make changes in your fork
3. Submit a [merge request](https://gitlab.com/ASTRELION/launchme/-/merge_requests/new)

## Issues & Feature Requests

The best way to report an issue would be to open a new issue [here](https://gitlab.com/ASTRELION/launchme/-/issues/new). Please try to be as descriptive as possible when reporting any issues :).

You may also [open an issue](https://gitlab.com/ASTRELION/launchme/-/issues/new) for new features you'd like to be added.

## Statistics

[<img src="https://bstats.org/signatures/bukkit/LaunchMe.svg" height="250" />](https://bstats.org/plugin/bukkit/LaunchMe/9918)

<a href="https://liberapay.com/ASTRELION/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>
