package com.astrelion.launchme.util;

/**
 * Static utility functions.
 */
public class Util
{
    /**
     * Clamp the given value between min and max.
     * @param value the value to clamp
     * @param min the minimum value
     * @param max the maximum value
     * @return the result value between min and max
     */
    public static double clamp(double value, double min, double max)
    {
        return Math.max(min, Math.min(max, value));
    }
}
