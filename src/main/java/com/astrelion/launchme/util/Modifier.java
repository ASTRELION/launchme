package com.astrelion.launchme.util;

public class Modifier
{
    /** The modifier number */
    private final double modNumber;
    /** Whether the modifier is a constant value or not */
    private final boolean isConst;

    /**
     * Construct a new Modifier.
     * @param modifierString the modifier in the form of "(+|-)\d+"
     */
    public Modifier(String modifierString)
    {
        isConst = !(modifierString.startsWith("+") || modifierString.startsWith("-"));
        modNumber = Double.parseDouble(modifierString);
    }

    /**
     * Apply this modifier to the given number.
     * @param number the number to apply this modifier to
     * @return the number + modifier
     */
    public double apply(double number)
    {
        if (isConst)
        {
            return modNumber;
        }
        else
        {
            return number + modNumber;
        }
    }
}
