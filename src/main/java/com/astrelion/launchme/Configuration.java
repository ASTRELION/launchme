package com.astrelion.launchme;

import com.astrelion.launchme.util.Modifier;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Projectile;

import java.util.ArrayList;
import java.util.List;

public class Configuration extends ResourceManager
{
    public static boolean ENABLED;
    public static boolean PROMPT_UPDATES;

    public static boolean TOGGLE_DEFAULT;
    public static boolean TOGGLE_DEFAULT_ALL;
    public static boolean DISABLE_WHEN_SNEAKING;
    public static boolean DISABLE_SMART;
    public static int COOLDOWN_TIME;
    public static boolean ENABLE_MOB_LAUNCHES;
    public static boolean SHOULD_RESET_VEHICLE;
    public static boolean ALLOW_MANUAL_DISMOUNT;
    public static boolean FORCED_DISMOUNT;
    public static boolean PLAY_SOUND;
    public static Sound SOUND;
    public static boolean SPAWN_PARTICLES;
    public static Particle PARTICLE;

    public static int XP_COST;
    public static int LEVEL_COST;
    public static int HUNGER_COST;
    public static int HEALTH_COST;
    public static boolean PREVENT_DEATH;
    public static Material ITEM_COST;
    public static int ITEM_COST_AMOUNT;
    public static boolean REQUIRE_ITEM_IN_OFFHAND;
    public static boolean REQUIRE_ITEM_IN_HOTBAR;
    public static boolean SOFT_COST;
    public static boolean MOB_COST;

    public static boolean HAS_GRAVITY;
    public static double VELOCITY_SCALAR;
    public static Modifier ARROW_KNOCKBACK_MODIFIER;
    public static Modifier ARROW_PIERCE_LEVEL;
    public static Modifier ARROW_DAMAGE_MODIFIER;
    public static boolean ARROW_ALLOW_PICKUP;
    public static List<EntityType> ENABLED_PROJECTILES;

    public static boolean ENABLE_TOGGLE_MESSAGE;
    public static boolean ENABLE_TOGGLE_PROMPT;
    public static boolean ENABLE_COOLDOWN_PROGRESS_PROMPT;
    public static boolean ENABLE_COOLDOWN_DONE_PROMPT;
    public static boolean ENABLE_LAUNCH_MESSAGE;

    public Configuration(LaunchMe plugin)
    {
        super(plugin, "config.yml");
        loadConfig();
    }

    /**
     * Read configuration values and load them into `this`.
     */
    public void loadConfig()
    {
        reload();

        ENABLED = configuration.getBoolean("enable");
        PROMPT_UPDATES = configuration.getBoolean("promptUpdates");

        TOGGLE_DEFAULT = configuration.getBoolean("toggleDefault");
        TOGGLE_DEFAULT_ALL = configuration.getBoolean("toggleDefaultAll");
        DISABLE_WHEN_SNEAKING = configuration.getBoolean("disableWhenSneaking");
        DISABLE_SMART = configuration.getBoolean("disableSmart");
        COOLDOWN_TIME = configuration.getInt("cooldownTime");
        ENABLE_MOB_LAUNCHES = configuration.getBoolean("enableMobLaunches");
        SHOULD_RESET_VEHICLE = configuration.getBoolean("shouldResetVehicle");
        ALLOW_MANUAL_DISMOUNT = configuration.getBoolean("allowManualDismount");
        FORCED_DISMOUNT = configuration.getBoolean("forcedDismount");
        PLAY_SOUND = configuration.getBoolean("playSound");
        SOUND = Sound.valueOf(configuration.getString("sound"));
        SPAWN_PARTICLES = configuration.getBoolean("spawnParticles");
        PARTICLE = Particle.valueOf(configuration.getString("particle"));

        XP_COST = configuration.getInt("xpCost");
        LEVEL_COST = configuration.getInt("levelCost");
        HUNGER_COST = configuration.getInt("hungerCost");
        HEALTH_COST = configuration.getInt("healthCost");
        PREVENT_DEATH = configuration.getBoolean("preventDeath");
        Object obj = configuration.get("itemCost");
        ITEM_COST = obj == null ? null : Material.matchMaterial(obj.toString());
        ITEM_COST_AMOUNT = configuration.getInt("itemCostAmount");
        REQUIRE_ITEM_IN_OFFHAND = configuration.getBoolean("requireItemInOffhand");
        REQUIRE_ITEM_IN_HOTBAR = configuration.getBoolean("requireItemInHotbar");
        SOFT_COST = configuration.getBoolean("softCost");
        MOB_COST = configuration.getBoolean("mobCost");

        HAS_GRAVITY = configuration.getBoolean("hasGravity");
        VELOCITY_SCALAR = configuration.getDouble("velocityScalar");
        ARROW_KNOCKBACK_MODIFIER = new Modifier(configuration.getString("arrowKnockbackModifier"));
        ARROW_PIERCE_LEVEL = new Modifier(configuration.getString("arrowPierceModifier"));
        ARROW_DAMAGE_MODIFIER = new Modifier(configuration.getString("arrowDamageModifier"));
        ARROW_ALLOW_PICKUP = configuration.getBoolean("arrowAllowPickup");
        ENABLED_PROJECTILES = new ArrayList<>();
        List<?> projectileNames = configuration.getList("launchableProjectiles");

        for (var p : projectileNames)
        {
            ENABLED_PROJECTILES.add(EntityType.valueOf(p.toString().toUpperCase()));
        }

        ENABLE_TOGGLE_MESSAGE = configuration.getBoolean("enableToggleMessage");
        ENABLE_TOGGLE_PROMPT = configuration.getBoolean("enableTogglePrompt");
        ENABLE_COOLDOWN_PROGRESS_PROMPT = configuration.getBoolean("enableCooldownProgressPrompt");
        ENABLE_COOLDOWN_DONE_PROMPT = configuration.getBoolean("enableCooldownDonePrompt");
        ENABLE_LAUNCH_MESSAGE = configuration.getBoolean("enableLaunchMessage");
    }

    /**
     * Check whether the given Projectile is an enabled Projectile.
     * @param projectile the Projectile to check
     * @return true if enabled, false otherwise
     */
    public boolean isProjectileEnabled(Projectile projectile)
    {
        return ENABLED_PROJECTILES.contains(projectile.getType());
    }

    /**
     * Check whether the given entity (must be a Projectile) is an enabled Projectile.
     * @param entity the Entity to check
     * @return true if enabled, false otherwise (or not a Projectile)
     */
    public boolean isProjectileEnabled(Entity entity)
    {
        if (!(entity instanceof Projectile)) return false;
        return isProjectileEnabled((Projectile) entity);
    }
}
