package com.astrelion.launchme;

import com.astrelion.launchme.commands.CommandManager;
import net.kyori.adventure.text.Component;
import org.bstats.bukkit.Metrics;
import org.bstats.charts.SingleLineChart;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

public class LaunchMe extends JavaPlugin
{
    private CommandManager commandManager;
    private Configuration configuration;
    private Permissions permissions;
    private Events events;
    private Language language;
    private Metrics metrics;

    /** Source release URL for update checks **/
    private final String SOURCE_URL = "https://gitlab.com/ASTRELION/launchme/-/releases/permalink/latest";

    public LaunchMe()
    {
        super();
    }

    /**
     * Signature required for MockBukkit.
     * @param loader
     * @param description
     * @param dataFolder
     * @param file
     */
    protected LaunchMe(JavaPluginLoader loader, PluginDescriptionFile description, File dataFolder, File file)
    {
        super(loader, description, dataFolder, file);
    }

    /**
     * Called when the plugin is enabled.
     */
    @Override
    public void onEnable()
    {
        // We need these for prompts and determining config values before anything else
        configuration = new Configuration(this);
        language = new Language(this);

        // Check for new update
        getServer().getScheduler().runTaskAsynchronously(this, () ->
        {
            if (Configuration.PROMPT_UPDATES && checkForUpdate())
            {
                for (Player player : getServer().getOnlinePlayers())
                {
                    if (player.hasPermission(Permissions.UPDATE))
                    {
                        player.sendMessage(Component.text(language.getMessage("outdated", SOURCE_URL)));
                    }
                }
            }
        });

        // Enable plugin
        if (Configuration.ENABLED)
        {
            permissions = new Permissions(this);
            commandManager = new CommandManager(this);
            events = new Events(this);

            // bStats
            final int bStatsID = 9918;
            metrics = new Metrics(this, bStatsID);
            metrics.addCustomChart(new SingleLineChart("projectiles_launched", () ->
                events.getAndResetProjectilesLaunched())
            );

            getLogger().info(language.getMessage("logEnabled"));
        }
        else
        {
            getLogger().warning(language.getMessage("logDisabledConfig"));
        }
    }

    /**
     * Called when the plugin is disabled.
     */
    @Override
    public void onDisable()
    {
        getLogger().warning(language.getMessage("logDisabled"));

        language = null;
        configuration = null;
        permissions = null;
        commandManager = null;
        events = null;
    }

    /**
     * Check this version against the most recent source version.
     * @return true if a new update is available, false otherwise.
     */
    private boolean checkForUpdate()
    {
        try
        {
            // Query URL
            URL url = new URL(SOURCE_URL);
            URLConnection connection = url.openConnection();
            connection.getInputStream();
            URL resolvedURL = connection.getURL();

            // Resolve version integer
            String[] path = resolvedURL.toString().split("/");
            String sourceVersion = path[path.length - 1];
            int sourceVersionInt = Integer.parseInt(sourceVersion.replaceAll("\\D", ""));

            // Get this version
            String thisVersion = getDescription().getVersion();
            int thisVersionInt = Integer.parseInt(thisVersion.replaceAll("\\D", ""));

            // Alert
            if (sourceVersionInt > thisVersionInt)
            {
                getLogger().warning(language.getMessage("logOld", thisVersion, sourceVersion, SOURCE_URL));
                return true;
            }
            else if (sourceVersionInt < thisVersionInt)
            {
                getLogger().info(language.getMessage("logNew", thisVersion, sourceVersion));
            }
            else
            {
                getLogger().info(language.getMessage("logCurrent", thisVersion));
            }
        }
        catch (IOException e)
        {
            getLogger().warning(language.getMessage("logUpdateFail"));
        }

        return false;
    }

    public Configuration getConfiguration()
    {
        return this.configuration;
    }

    public Permissions getPermissions()
    {
        return this.permissions;
    }

    public CommandManager getCommandManager()
    {
        return this.commandManager;
    }

    public Events getEventController()
    {
        return this.events;
    }
    public Language getLanguage()
    {
        return this.language;
    }
}
