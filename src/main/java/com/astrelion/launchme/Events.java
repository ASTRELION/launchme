package com.astrelion.launchme;

import com.astrelion.launchme.util.Util;
import net.kyori.adventure.text.Component;
import org.bukkit.FluidCollisionMode;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.RayTraceResult;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

public class Events implements Listener
{
    private final LaunchMe plugin;
    private final Permissions permissions;
    private final Configuration configuration;
    private final Language language;

    private int projectilesLaunched;

    public Events(LaunchMe plugin)
    {
        this.plugin = plugin;
        this.permissions = plugin.getPermissions();
        this.configuration = plugin.getConfiguration();
        this.language = plugin.getLanguage();

        projectilesLaunched = 0;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    /**
     * Resets the projectile launch counter and returns the current value.
     * @return the current projectilesLaunched value
     */
    public int getAndResetProjectilesLaunched()
    {
        int temp = projectilesLaunched;
        projectilesLaunched = 0;
        return temp;
    }

    /**
     * PlayerJoinEvent.
     * Handles default toggling for Players.
     * @param event the PlayerJoinEvent
     */
    @EventHandler
    private void onPlayerJoinEvent(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();

        if (Configuration.TOGGLE_DEFAULT_ALL ||
            (Configuration.TOGGLE_DEFAULT && player.hasPermission(Permissions.TOGGLE)))
        {
            permissions.togglePlayer(player, true);
            player.sendMessage(Component.text(language.getMessage("launchmeToggle", true)));
        }
    }

    /**
     * ProjectileLaunchEvent.
     * Handles Projectile riding (+ checks, permissions, etc.)
     * @param event the ProjectileLaunchEvent
     */
    @EventHandler
    private void onProjectileLaunchEvent(ProjectileLaunchEvent event)
    {
        Projectile projectile = event.getEntity();

        if (projectile.getShooter() instanceof LivingEntity shooter &&
            configuration.isProjectileEnabled(projectile))
        {
            boolean success = rideProjectile(shooter, projectile);

            // Put player on cooldown
            if (success && shooter instanceof Player player)
            {
                permissions.putPlayerOnCooldown(player);
            }

            // Play effects and modify projectile
            if (success)
            {
                projectile.setGravity(Configuration.HAS_GRAVITY);
                projectile.setVelocity(projectile.getVelocity().multiply(Configuration.VELOCITY_SCALAR));

                if (projectile instanceof AbstractArrow arrow)
                {
                    arrow.setDamage(
                        Configuration.ARROW_DAMAGE_MODIFIER.apply(arrow.getDamage())
                    );
                    arrow.setPierceLevel(
                        (int) Util.clamp(Configuration.ARROW_PIERCE_LEVEL.apply(arrow.getPierceLevel()), 0, 127)
                    );
                    arrow.setKnockbackStrength(
                        (int) Configuration.ARROW_KNOCKBACK_MODIFIER.apply(arrow.getKnockbackStrength())
                    );
                    arrow.setPickupStatus(
                        Configuration.ARROW_ALLOW_PICKUP ? AbstractArrow.PickupStatus.ALLOWED : AbstractArrow.PickupStatus.DISALLOWED
                    );
                }

                if (Configuration.PLAY_SOUND)
                {
                    shooter.getWorld().playSound(
                        shooter.getLocation(),
                        Configuration.SOUND,
                        16,
                        1
                    );
                }

                if (Configuration.SPAWN_PARTICLES)
                {
                    Vector velocity = projectile.getVelocity().normalize();

                    shooter.getWorld().spawnParticle(
                        Configuration.PARTICLE,
                        shooter.getLocation(),
                        25,
                        -velocity.getX(),
                        -velocity.getY(),
                        -velocity.getZ(),
                        0.2
                    );
                }
            }
        }
    }

    /**
     * ProjectileHitEvent.
     * Handles automatically dismounting entities.
     * @param event the ProjectileHitEvent
     */
    @EventHandler
    private void onProjectileHitEvent(ProjectileHitEvent event)
    {
        Projectile projectile = event.getEntity();
        ProjectileSource source = projectile.getShooter();

        if (Configuration.FORCED_DISMOUNT &&
            source instanceof LivingEntity livingEntity &&
            projectile.getPassengers().contains(source) &&
            configuration.isProjectileEnabled(projectile))
        {
            projectile.eject();
        }
    }

    /**
     * EntityDismountEvent.
     * Handles preventing dismounting.
     * @param event the EntityDismountEvent
     */
    @EventHandler
    private void onEntityDismountEvent(EntityDismountEvent event)
    {
        Entity entity = event.getEntity();
        Entity dismounted = event.getDismounted();

        NamespacedKey key = new NamespacedKey(plugin, "launcher");
        entity.getPersistentDataContainer().set(key, PersistentDataType.STRING, entity.getUniqueId().toString());

        if (!Configuration.ALLOW_MANUAL_DISMOUNT &&
            entity instanceof Player player &&
            permissions.isPlayerToggled(player) &&
            configuration.isProjectileEnabled(dismounted))
        {
            event.setCancelled(true);
        }
    }

    /**
     * Set the given Projectile as the vehicle for the Entity if all conditions are met.
     * @param entity the LivingEntity (Player or Monster)
     * @param projectile the Projectile
     * @return true if the "ride" was successful, false otherwise
     */
    private boolean rideProjectile(LivingEntity entity, Projectile projectile)
    {
        // Mobs enabled check
        if (!Configuration.ENABLE_MOB_LAUNCHES && entity instanceof Monster)
        {
            return false;
        }

        // Player-specific checks
        if (entity instanceof Player player)
        {
            // Check cooldown and permissions
            if (!permissions.canLaunch(player))
            {
                if (Configuration.ENABLE_COOLDOWN_PROGRESS_PROMPT &&
                    permissions.isPlayerOnCooldown(player))
                {
                    player.sendMessage(Component.text(language.getMessage("onCooldown")));
                }
                else if (Configuration.ENABLE_TOGGLE_PROMPT &&
                    permissions.playerHasToggle(player))
                {
                    player.sendMessage(Component.text(language.getMessage("promptToggle")));
                }

                return false;
            }

            // Check sneaking
            if (Configuration.DISABLE_WHEN_SNEAKING && player.isSneaking())
            {
                return false;
            }

            // Check smart raycast
            if (Configuration.DISABLE_SMART)
            {
                int maxDistance = plugin.getServer().getViewDistance() * 16;

                RayTraceResult blockResult = player.getWorld().rayTraceBlocks(
                    player.getLocation(),
                    player.getEyeLocation().getDirection(),
                    maxDistance,
                    FluidCollisionMode.ALWAYS,
                    false
                );

                if (blockResult != null &&
                    blockResult.getHitBlock() != null &&
                    (blockResult.getHitBlock().getType() == Material.WATER || blockResult.getHitBlock().getType() == Material.LAVA))
                {
                    return false;
                }

                RayTraceResult entityResult = player.getWorld().rayTraceEntities(
                    player.getLocation(),
                    player.getEyeLocation().getDirection(),
                    maxDistance,
                    1,
                    e -> (e instanceof LivingEntity && e.getEntityId() != player.getEntityId())
                );

                if (entityResult != null &&
                    entityResult.getHitEntity() != null &&
                    entityResult.getHitEntity() instanceof Monster)
                {
                    return false;
                }
            }
        }

        ItemStack itemstack = null;
        if ((entity instanceof Monster && Configuration.MOB_COST) ||
            (entity instanceof Player player && !permissions.hasNoCost(player)))
        {
            // XP cost
            if (Configuration.XP_COST > 0 &&
                entity instanceof Player player)
            {
                if ((Configuration.SOFT_COST && player.getTotalExperience() < 1) ||
                    (!Configuration.SOFT_COST && player.getTotalExperience() < Configuration.XP_COST))
                {
                    return false;
                }
            }

            // Level cost
            if (Configuration.LEVEL_COST > 0 &&
                entity instanceof Player player)
            {
                if ((Configuration.SOFT_COST && player.getLevel() < 1) ||
                        (!Configuration.SOFT_COST && player.getLevel() < Configuration.LEVEL_COST))
                {
                    return false;
                }
            }

            // Hunger cost
            if (Configuration.HUNGER_COST > 0 &&
                entity instanceof Player player)
            {
                if ((Configuration.SOFT_COST && player.getFoodLevel() < 1) ||
                        (!Configuration.SOFT_COST && player.getFoodLevel() < Configuration.HUNGER_COST))
                {
                    return false;
                }
            }

            // Health cost
            if (Configuration.HEALTH_COST > 0)
            {
                if ((Configuration.PREVENT_DEATH && entity.getHealth() - Configuration.HEALTH_COST <= 0) ||
                    (Configuration.SOFT_COST && entity.getHealth() < 1) ||
                    (!Configuration.SOFT_COST && entity.getHealth() < Configuration.HEALTH_COST))
                {
                    return false;
                }
            }

            // Item cost
            if (Configuration.ITEM_COST_AMOUNT > 0 &&
                Configuration.ITEM_COST != null &&
                Configuration.ITEM_COST != Material.AIR)
            {
                EntityEquipment equipment = entity.getEquipment();

                // Check offhand item
                if (equipment != null)
                {
                    ItemStack item = equipment.getItemInOffHand();

                    if (item.getType().equals(Configuration.ITEM_COST))
                    {
                        if ((Configuration.SOFT_COST && item.getAmount() < 1) ||
                            (!Configuration.SOFT_COST && item.getAmount() < Configuration.ITEM_COST_AMOUNT))
                        {
                            return false;
                        }

                        itemstack = item;
                    }
                    else if (Configuration.REQUIRE_ITEM_IN_OFFHAND && !Configuration.REQUIRE_ITEM_IN_HOTBAR)
                    {
                        return false;
                    }
                }

                // Otherwise, check hotbar, then inventory
                if (entity instanceof Player player && itemstack == null)
                {
                    // Check hotbar
                    ItemStack item;
                    for (int i = 0; i < 9; i++)
                    {
                        item = player.getInventory().getItem(i);
                        if (item != null && item.getType() == Configuration.ITEM_COST)
                        {
                            itemstack = item;
                            break;
                        }
                    }

                    if (itemstack == null && Configuration.REQUIRE_ITEM_IN_HOTBAR)
                    {
                        return false;
                    }

                    // Check inventory
                    if (itemstack == null)
                    {
                        int i = player.getInventory().first(Configuration.ITEM_COST);
                        if (i < 0) return false;
                        ItemStack invItem = player.getInventory().getItem(i);
                        if (invItem == null || invItem.getType() == Material.AIR) return false;

                        if ((Configuration.SOFT_COST && invItem.getAmount() < 1) ||
                            (!Configuration.SOFT_COST && invItem.getAmount() < Configuration.ITEM_COST_AMOUNT))
                        {
                            return false;
                        }

                        itemstack = invItem;
                    }
                }
            }

            // Apply player costs
            if (entity instanceof Player player)
            {
                if (Configuration.XP_COST > 0)
                {
                    player.setTotalExperience(Math.max(player.getTotalExperience() - Configuration.XP_COST, 0));
                }

                if (Configuration.LEVEL_COST > 0)
                {
                    player.setLevel(Math.max(player.getLevel() - Configuration.LEVEL_COST, 0));
                }

                if (Configuration.HUNGER_COST > 0)
                {
                    player.setFoodLevel(Math.max(player.getFoodLevel() - Configuration.HUNGER_COST, 0));
                }
            }

            // Apply health costs
            if (Configuration.HEALTH_COST > 0)
            {
                entity.setHealth(Math.max(entity.getHealth() - Configuration.HEALTH_COST, 0));
            }

            // Apply item costs
            if (Configuration.ITEM_COST != null && Configuration.ITEM_COST_AMOUNT > 0)
            {
                if (itemstack != null)
                {
                    itemstack.setAmount(Math.max(itemstack.getAmount() - Configuration.ITEM_COST_AMOUNT, 0));
                }
            }
        }

        // Current vehicle check
        if (entity.isInsideVehicle())
        {
            if (Configuration.SHOULD_RESET_VEHICLE)
            {
                entity.getVehicle().eject();
            }
            else
            {
                return false;
            }
        }

        // Finally launch the entity
        projectile.addPassenger(entity);
        projectilesLaunched++;

        if (entity instanceof Player && Configuration.ENABLE_LAUNCH_MESSAGE)
        {
            entity.sendMessage(Component.text(language.getMessage("riding", projectile.getName())));
        }

        return true;
    }
}
