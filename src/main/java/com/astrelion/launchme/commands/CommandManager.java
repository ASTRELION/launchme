package com.astrelion.launchme.commands;

import co.aikar.commands.PaperCommandManager;
import com.astrelion.launchme.LaunchMe;

public class CommandManager
{
    private final LaunchMe plugin;
    private final PaperCommandManager manager;

    public CommandManager(LaunchMe plugin)
    {
        this.plugin = plugin;
        this.manager = new PaperCommandManager(plugin);
        manager.enableUnstableAPI("help");

        manager.registerCommand(new LaunchMeCommand(plugin));
    }
}
