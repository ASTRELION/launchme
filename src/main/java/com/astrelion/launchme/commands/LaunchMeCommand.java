package com.astrelion.launchme.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import com.astrelion.launchme.Configuration;
import com.astrelion.launchme.Language;
import com.astrelion.launchme.LaunchMe;
import com.astrelion.launchme.Permissions;
import net.kyori.adventure.text.Component;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFactory;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.ScoreboardManager;

@CommandAlias("launchme|lm")
public class LaunchMeCommand  extends BaseCommand
{
    private final LaunchMe plugin;
    private final Configuration configuration;
    private final Language language;
    private final Permissions permissions;

    @Dependency
    private JavaPlugin genericPlugin;
    @Dependency
    private PluginManager pluginManager;
    @Dependency
    private Server server;
    @Dependency
    private BukkitScheduler bukkitScheduler;
    @Dependency
    private ScoreboardManager scoreboardManager;
    @Dependency
    private ItemFactory itemFactory;
    @Dependency
    private PluginDescriptionFile description;

    public LaunchMeCommand(LaunchMe plugin)
    {
        super();
        this.plugin = plugin;
        this.configuration = plugin.getConfiguration();
        this.language = plugin.getLanguage();
        this.permissions = plugin.getPermissions();
    }

    @HelpCommand
    @CommandPermission(Permissions.HELP)
    public void doHelp(CommandSender sender, CommandHelp help)
    {
        help.showHelp();
    }

    @Subcommand("reload")
    @Description("Reload configurations from file.")
    @CommandPermission(Permissions.RELOAD)
    public void doReload(CommandSender sender)
    {
        configuration.loadConfig();
        language.reload();
        sender.sendMessage(Component.text(language.getMessage("launchmeReload")));
    }

    @Subcommand("toggle")
    @Description("Toggle launching for yourself or the given player.")
    @CommandCompletion("@players")
    public void doToggle(CommandSender sender, @Optional String player)
    {
        Player other = server.getPlayer(player != null ? player : sender.getName());
        Player target = null;

        // Check permissions
        if (sender instanceof Player source)
        {
            // default target to the sender
            if (source.hasPermission(Permissions.TOGGLE)) target = source;
            // try to set target player to another player
            if (other != null && other != source && source.hasPermission(Permissions.TOGGLE_OTHER)) target = other;
        }
        else
        {
            target = other;
        }

        // Toggle target player
        if (target != null)
        {
            permissions.togglePlayer(target);
            boolean toggled = permissions.isPlayerToggled(target);

            if (Configuration.ENABLE_TOGGLE_MESSAGE)
            {
                target.sendMessage(Component.text(language.getMessage("launchmeToggle", toggled)));
                if (!sender.getName().equalsIgnoreCase(target.getName()))
                {
                    sender.sendMessage(Component.text(language.getMessage("launchmeToggleOther", toggled).formatted(target.getName())));
                }
            }
        }
        else
        {
            sender.sendMessage(Component.text(language.getMessage("noPermission")));
        }
    }
}
