package com.astrelion.launchme;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class ResourceManager
{
    protected final LaunchMe plugin;
    protected final String resourceName;
    protected YamlConfiguration configuration;

    public ResourceManager(LaunchMe plugin, String resourceName)
    {
        this.plugin = plugin;
        this.resourceName = resourceName;

        updateResource();
    }

    /**
     * If the existing resource is outdated, save a new version with old values.
     * If the resource doesn't exist, saves the default configuration.
     */
    public void updateResource()
    {
        File file = new File(plugin.getDataFolder(), resourceName);

        // Replace existing file
        if (file.exists())
        {
            // Store old config
            YamlConfiguration yamlConfiguration = YamlConfiguration.loadConfiguration(file);
            Map<String, Object> values = yamlConfiguration.getValues(true);

            // Save a default config
            plugin.saveResource(resourceName, true);
            yamlConfiguration = YamlConfiguration.loadConfiguration(file);

            // Load default config with old values, if the keys still exist
            for (String key : yamlConfiguration.getValues(true).keySet())
            {
                if (values.containsKey(key))
                {
                    yamlConfiguration.set(key, values.get(key));
                }
            }

            // Commit
            try
            {
                yamlConfiguration.save(file);
            }
            catch (IOException e)
            {
                throw new RuntimeException(e);
            }
            this.configuration = yamlConfiguration;
        }
        // Save default file
        else
        {
            plugin.saveResource(resourceName, false);
            this.configuration = YamlConfiguration.loadConfiguration(file);
        }
    }

    /**
     * Reload the configuration from file.
     */
    public void reload()
    {
        File file = new File(plugin.getDataFolder(), resourceName);
        this.configuration = YamlConfiguration.loadConfiguration(file);
    }

    public YamlConfiguration getConfiguration()
    {
        return this.configuration;
    }
}
