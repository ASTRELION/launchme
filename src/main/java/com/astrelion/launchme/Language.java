package com.astrelion.launchme;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Language extends ResourceManager
{
    public Language(LaunchMe plugin)
    {
        super(plugin, "lang.yml");
    }

    public String getMessage(String key, boolean which)
    {
        String message = getMessage(key);

        // match {|}
        Pattern pattern = Pattern.compile("\\{.*?\\|.*?\\}");
        Matcher matcher = pattern.matcher(message);

        // find each {|} and replace it with one of either side of |
        while (matcher.find())
        {
            String group = matcher.group().replaceAll("\\{|\\}", "");
            String[] split = group.split("\\|");
            message = message.replaceFirst("\\{.*?\\|.*?\\}", split[which ? 0 : 1]);

            matcher = pattern.matcher(message);
        }

        return message;
    }

    public String getMessage(String key)
    {
        return configuration.getString(key);
    }

    public String getMessage(String key, Object... args)
    {
        return getMessage(key).formatted(args);
    }
}
