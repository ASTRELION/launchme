package com.astrelion.launchme;

import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class Permissions
{
    public static final String HELP = "launchme.help";
    public static final String NO_COOLDOWN = "launchme.nocooldown";
    public static final String NO_COST = "launchme.nocost";
    public static final String RELOAD = "launchme.reload";
    public static final String TOGGLE = "launchme.toggle";
    public static final String TOGGLE_OTHER = "launchme.toggle.other";
    public static final String UPDATE = "launchme.update";

    private final Set<UUID> toggledPlayers;
    private final Set<UUID> onCooldownPlayers;
    private final LaunchMe plugin;
    private final Language language;

    public Permissions(LaunchMe plugin)
    {
        this.plugin = plugin;
        this.language = plugin.getLanguage();

        toggledPlayers = new HashSet<>();
        onCooldownPlayers = new HashSet<>();
    }

    /**
     * Determine if given player can launch themselves
     * @param player the Player to check
     * @return True if the player can launch
     */
    public boolean canLaunch(Player player)
    {
        return
            !isPlayerOnCooldown(player) &&
            isPlayerToggled(player);
    }

    public boolean isPlayerToggled(Player player)
    {
        return toggledPlayers.contains(player.getUniqueId());
    }

    public boolean isPlayerOnCooldown(Player player)
    {
        return onCooldownPlayers.contains(player.getUniqueId());
    }

    public void putPlayerOnCooldown(Player player)
    {
        if (!isPlayerOnCooldown(player))
        {
            if (!player.hasPermission(NO_COOLDOWN))
            {
                this.onCooldownPlayers.add(player.getUniqueId());

                // remove cooldown from player after seconds
                plugin.getServer().getScheduler().runTaskLaterAsynchronously(
                    plugin,
                    () -> removeCooldown(player),
                    Configuration.COOLDOWN_TIME * 20L
                );
            }
            else
            {
                removeCooldown(player);
            }
        }
        else if (Configuration.ENABLE_COOLDOWN_PROGRESS_PROMPT)
        {
            player.sendMessage(Component.text(language.getMessage("onCooldown")));
        }
    }

    public void removeCooldown(Player player)
    {
        if (isPlayerOnCooldown(player))
        {
            this.onCooldownPlayers.remove(player.getUniqueId());

            if (Configuration.ENABLE_COOLDOWN_DONE_PROMPT)
            {
                player.sendMessage(Component.text(language.getMessage("offCooldown")));
            }
        }
    }

    public void togglePlayer(Player player)
    {
        if (toggledPlayers.contains(player.getUniqueId()))
        {
            toggledPlayers.remove(player.getUniqueId());
        }
        else
        {
            toggledPlayers.add(player.getUniqueId());
        }
    }

    public void togglePlayer(Player player, boolean toggle)
    {
        if (toggle)
        {
            toggledPlayers.add(player.getUniqueId());
        }
        else
        {
            toggledPlayers.remove(player.getUniqueId());
        }
    }

    public boolean hasNoCost(Player player)
    {
        return player.hasPermission(NO_COST);
    }

    public boolean playerHasToggle(Player player)
    {
        return player.hasPermission(TOGGLE);
    }
}
