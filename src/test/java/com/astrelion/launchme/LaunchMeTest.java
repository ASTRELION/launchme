//package com.astrelion.launchme;
//
//import be.seeseemelk.mockbukkit.MockBukkit;
//import be.seeseemelk.mockbukkit.ServerMock;
//import be.seeseemelk.mockbukkit.entity.PlayerMock;
//import org.junit.jupiter.api.AfterAll;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//
//public class LaunchMeTest
//{
//    private static LaunchMe plugin;
//    private static ServerMock server;
//
//    @BeforeAll
//    static void beforeAll()
//    {
//        server = MockBukkit.mock();
//        plugin = MockBukkit.load(LaunchMe.class);
//    }
//
//    @AfterAll
//    static void afterAll()
//    {
//        MockBukkit.unmock();
//    }
//
//    @Test
//    public void permissionTest()
//    {
//        Permissions permissions = plugin.getPermissions();
//
//        PlayerMock player = server.addPlayer();
//
//        // No permission player
//        Assertions.assertFalse(permissions.canLaunch(player));
//        Assertions.assertFalse(permissions.playerHasToggle(player));
//        Assertions.assertFalse(permissions.hasNoCost(player));
//        Assertions.assertFalse(permissions.isPlayerOnCooldown(player));
//        Assertions.assertFalse(permissions.isPlayerToggled(player));
//
//        //permissions.putPlayerOnCooldown(player);
//        //Assertions.assertTrue(permissions.isPlayerOnCooldown(player));
//        //permissions.removeCooldown(player);
//        //Assertions.assertFalse(permissions.isPlayerOnCooldown(player));
//
//        // All permission player
//        player.setOp(true);
//
//        Assertions.assertFalse(permissions.canLaunch(player));
//        Assertions.assertTrue(permissions.playerHasToggle(player));
//        Assertions.assertTrue(permissions.hasNoCost(player));
//        Assertions.assertFalse(permissions.isPlayerToggled(player));
//
//        permissions.togglePlayer(player);
//
//        Assertions.assertTrue(permissions.isPlayerToggled(player));
//        Assertions.assertTrue(permissions.canLaunch(player));
//
//        System.out.println("we be runnin");
//    }
//}
